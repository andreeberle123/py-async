
from greenlet import greenlet

import socket
import requests
import errno
import ssl
import time
import logging
import json
import smtplib
import os
import zlib

from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEBase import MIMEBase
from email import encoders

from collections import deque
from urlparse import urlparse
from http_parser.http import HttpParser

CONST_STAGE_INITIAL = 0
CONST_STAGE_CONNECT = 0
CONST_STAGE_ = 0

HTTP_PORT = 80
HTTPS_PORT = 443

sock_data = dict()
persisted_hosts = dict()

class Sockdata(object):
    """
    Generic object to store the associated request info between
    calls.
    """
    def __init__(self):
        self._ssl = False
        self._handshake = False



class SockReuse(object):
    def __init__(self, sck):
        self._when = int(time.time())
        self._sck = sck


    def age(self):
        return int(time.time()) - self._when



class InternalResponse(object):
    def __init__(self):
        pass



def _async_stage_0(async_info):
    """
    stage 0 is the initial stage for the async request processor.
    This function should not be called outside this module.
    This function will start an HTTP asynchronous request toward
    an url, contained inside the async_info parameter object.
    It performs the parsing of the url, and prepares the entire
    HTTP packet for forwarding. It will also create sockets
    and wraps appropriately in SSL if an HTTPS is requested.

    After preparing all data, this function will queue the async_info job
    object in the lasyncio engine, which will forward I/O results from the
    back into this module.
    """
    global persisted_hosts
    logging.debug('stage 0')
    url = async_info.url
    params = async_info.params

    if url == '':
        logging.error('Invalid URL on async stage 0')
        return

    ht = urlparse(url)

    if ht.port:
        port = ht.port
    else:
        port = HTTP_PORT

    usessl = False

    if ht.scheme == "https":
        # XXX: Atencao aqui, este metodo nao existe em algumas versoes do
        #      modulo ssl.
        context = ssl.create_default_context()
        port = HTTPS_PORT
        usessl = True

    if async_info.method == 'get':
        req = requests.Request(method='GET', url=url, params=params,
                               data="").prepare()
    else:
        if async_info.method == 'soap':
            url += '?wsdl'

        if async_info.method == 'post_file':
            req = requests.Request(method='POST',url=url, files=params.get('file')).prepare()
        else:
            req = requests.Request(method='POST', url=url, data=params).prepare()

    logging.debug('sending request to %s', url)
    if async_info.method != 'post_file':
        req.headers = {
            'Host': ht.netloc,
            'User-Agent': 'supernova',
            'Connection': 'keep-alive',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Language': 'en-US,en;q=0.5'
        }

        if async_info.method != 'soap':
            req.headers['Accept-Encoding'] = 'gzip, deflate'

        if async_info.method == 'post':
            req.headers['Content-type'] = 'application/json'
            req.headers['Content-Length'] = len(json.dumps(params))
            req.body = json.dumps(params)
        elif async_info.method == 'soap':
            req.headers['Content-type'] = 'text/xml'
            req.headers['Content-Length'] = len(params)
            req.body = params

    reqst = '{}\r\n{}\r\n\r\n'.format(
            req.method + ' ' + req.path_url + ' HTTP/1.1',
            '\r\n'.join('{}: {}'
                  .format(k, v) for k, v in req.headers.items())
    )

    if req.body:
        reqst = reqst + req.body

    domain = ht.hostname

    if ht.hostname not in persisted_hosts:
        persisted_hosts[ht.hostname] = deque()

    reuse_sck = False

    if persisted_hosts[ht.hostname]:
        # NOTE we only have 1 thread here, if we multithread on this system
        # we must lock this
        reuse_sck = True

    if reuse_sck:
        sr = persisted_hosts[ht.hostname].pop()
        logging.debug("reusing socket with age %d", sr.age())
        s = sr._sck
    else:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.setblocking(0)

        if usessl:
            s = context.wrap_socket(s, server_hostname=ht.netloc,
                                    do_handshake_on_connect=False)

    try:
        sock_data[s.fileno()] = Sockdata()
        sock_data[s.fileno()].senddata = reqst
        sock_data[s.fileno()].job = async_info
        sock_data[s.fileno()].parser = HttpParser()
        sock_data[s.fileno()].body = ''
        sock_data[s.fileno()].host = ht.hostname
        sock_data[s.fileno()].connected = False
        sock_data[s.fileno()].idx = 0

        if usessl:
            logging.debug('starting https')
            sock_data[s.fileno()]._ssl = True

        if reuse_sck:
            s.send(reqst)
            async_info.async.get_lio().push_socket(s, async_io_recv_callback,
                                                   True,
                                                   error_cb=async_io_err_callback)
        else:
            s.connect((domain, port))
            async_info.async.get_lio().push_socket(s, async_io_connect_callback,
                                                   False)

        async_info.defer = True
        return async_info
    except socket.error, c:
        errorcode = c[0]

        if errorcode == errno.EINPROGRESS: # connection in progress
            async_info.async.get_lio().push_socket(s, async_io_connect_callback,
                                                   False)

            async_info.defer = True
            return async_info

        logging.error("Error in socket %d", errorcode)

    return None



def _async_stage_final(job_info):
    """
    final stage for the request process. This method is gated from
    async_http_json(), and is used to return the final result (request
    response) back to the client.
    """
    logging.debug('response %s', job_info.result.__class__.__name__)
    logging.debug('final stage')
    job_info.defer = False
    _clear_http_async_data(job_info)
    return job_info



def get_job_and_remove(fno):
    """
    retrieves a SocketData from the current job storage, and remove it
    afterwards.
    """
    job = sock_data[fno].job
    del sock_data[fno]
    return job



def _async_email(async_info):
    params = async_info.params
    smtp_server = params.get('server')
    smtp_port = params.get('port')
    body = params.get('body')
    subject = params.get('subject')
    target = params.get('target')
    email_from_addr = params.get('from')
    email_user = params.get('user')
    email_pass = params.get('pass')
    attachment = params.get('attachment')

    try:
        msg = MIMEMultipart()
        msg['From'] = email_from_addr
        msg['To'] = target
        msg['Subject'] = subject
        msg.attach(MIMEText(body.encode('utf-8'), 'html', 'utf-8'))

        if attachment != None and len(attachment) > 0:
            file = open(attachment, 'rb')
            part = MIMEBase('application', 'octet-stream')
            part.set_payload(file.read())
            encoders.encode_base64(part)
            part.add_header('Content-Disposition',
                            'attachment; filename=%s' % os.path.basename(attachment))

            msg.attach(part)

        server = smtplib.SMTP('%s:%s' % (smtp_server, smtp_port))
        server.ehlo()
        server.starttls()
        server.login(email_user, email_pass)
        ret = server.sendmail(email_from_addr, target, msg.as_string())
        server.quit()

        if ret == {}:
            code = 200
            text = 'Ok'
        else:
            missing = [k for k in ret]
            code = -1
            text = 'Nao foi possivel enviar email para o(s) endereco(s):' + \
                    ','.join(k)
    except Exception as error:
        code = -2
        text = str(error)

    async_info.defer = False
    async_info.result = InternalResponse()
    async_info.result.text = text
    async_info.result.status_code = code
    _clear_http_async_data(async_info)

    return async_info



def async_http_json(method, url, params):
    """
    Function for asynchronous HTTP. This function will start an async HTTP
    GET or POST request to the param url. params is a dictionary with all
    GET/POST params. job_info is the associated job object, obtained from
    the ppool service threads. method must be a lowercase string with
    'get' or 'post'. 
    """
    job_info =  greenlet.getcurrent().get_job()
    job_info.url = url
    job_info.params = params
    job_info.method = method
    logging.debug("Async started to %s" % url)

    if job_info.http_async_handler == None:
        if method == 'email':
            job_info.http_async_handler = _async_email
        else:
            job_info.http_async_handler = _async_stage_0

    job_info.defer = True

    while True:
        # lock here, before getting into the handler, since
        # the i/o thread will modify its fields, and the process is started
        # during some of the handlers
        # FIXME is this bottlenecking the i/o thread? if so, rethink this
        job_info.lock()
        job_info = job_info.http_async_handler(job_info)
        if (job_info.defer):
            job_info.unlock()
            job_info.job_yield()
        else:
            job_info.unlock()
            return job_info.result

    #return job_info.http_asynchandler(job_info)

def _clear_http_async_data(job_info):
    job_info.http_async_handler = None
    #job_info.callback = None

def async_forward_error_job(sckt):
    """
    removes a job from the queue (identified by a socket), and requeues it
    with an empty result. This signals the request caller that the request
    failed
    """
    logging.error('forwarding error job')
    job2 = get_job_and_remove(sckt.fileno())
    job = job2.clone()
    sckt.close()
    job.defer = False
    job.result = None
    job.http_async_handler = _async_stage_final
    job.async.queue_job(job)



def async_io_connect_callback(scket, errcode):
    """
    The connection callback. The socket poll engine will call this
    callback when the socket finishes connecting, or fails during
    the process. If we failed to connect, we queue the original job back
    with an empty result, to signal we failed. If we succeeded, we proceed
    as follows:
    - If its a regular connection, we simply send the original HTTP request
      data
    - If its an HTTPS connecion, we start the handshake process. The handshake
      is run asynchronously, so we will wait for its results on the recv
      callback. However, the handshake might (unlikely) finish immediatly. In
      this case we forward the data immediatly.
    """
    logging.debug('conn cb')

    if errcode != 0:
        # connection error
        logging.error('Falha de conexao. error %i', errcode)
        async_forward_error_job(scket)
    else:
        sd = sock_data[scket.fileno()]
        dosend = False
        sd.connected = True

        if sd._ssl:
            if not sd._handshake:
                # comeca o handshake, que so ira acabar posteriormente
                try:
                    logging.debug("Starting handshake")
                    scket.do_handshake()
                    sd._handshake = True
                    dosend = True
                except ssl.SSLError:
                    #FIXME verificar se o erro foi want read
                    pass
                except Exception as e:
                    logging.error(e)
                    # FIXME warn caller
                    return True
            else:
                dosend = True
        else:
            # a conexao foi bem sucedida, envia os dados e reinsere o socket
            # para ouvir com nova callback
            logging.debug('connect ok, sending %s',
                          sock_data[scket.fileno()].senddata)

            dosend = True

        if dosend:
            res = scket.send(sd.senddata[sd.idx:])
            sd.idx = sd.idx + res
            if sd.idx < len(sd.senddata):
                # still more data to send
                logging.debug('more data to send')
                return False

        sd.job.async.get_lio().push_socket(scket, async_io_recv_callback, True,
                                           error_cb=async_io_err_callback)

        return True



def async_io_err_callback(scket, errcode):
    """
    Error callback, called by the poll engine when there is an error during the
    recv phase. If the error is ssl want more data, we check if we are doing
    the handshake. If we are, we call the do_handshake() again to see if it has
    concluded. If not, we ignore the error and return to the caller.

    Other errors cause a failure in the request, and is forwarded to the job queue
    """
    logging.debug("err callback %d", errcode)
    sd = sock_data[scket.fileno()]

    if sd._ssl:
        if not sd._handshake:
            try:
                logging.debug('Trying handshake')
                scket.do_handshake()
                sd._handshake = True
                logging.debug('handshake complete')
                res = scket.send(sd.senddata)
                sd.idx = sd.idx + res
                if sd.idx < len(sd.senddata):
                    logging.debug('more to send')
                    # still more data to send
                    return False
                else:
               #     sd.job.async.get_lio().push_socket(scket, async_io_recv_callback, True,
                #                           error_cb=async_io_err_callback)
                    return False

            except ssl.SSLError:
                #FIXME verificar se o erro foi want read
                return False
        else:
            # FIXME verificar se o erro foi want read
            # nao leu o suficiente, ignora e espera a proxima leitura
            return False
    else:
        logging.error("Error handling async request %d", errcode)
        async_forward_error_job(scket)

    return False



def async_io_recv_callback(scket, ret):
    """
    IO callback for data received in the socket endpoint. This function will
    conclude the request, and queue the job
    """
    sd = sock_data[scket.fileno()]
    logging.debug("recv callback %d", len(ret))

    if len(ret) == 0:
        logging.error('Invalid ret')
        job2 = get_job_and_remove(scket.fileno())
        job = job2
        job.lock()
        job.http_async_handler = _async_stage_final
        scket.close()
        job.result = None
        job.unlock()
        job.async.queue_job(job)
        return

    sd.parser.execute(ret, len(ret))
    p = sd.parser

    if p.is_partial_body():
        b = p.recv_body()
        sd.body = sd.body + b

    if p.is_message_complete():
        # message is finished
        b = p.recv_body()
        sd.body = sd.body + b
        job2 = get_job_and_remove(scket.fileno())
        headers=p.get_headers()
        if 'gzip' in headers.get('Transfer-Encoding','') or 'gzip' in headers.get('Content-Encoding',''):
            ''' O range abaixo está relacionado aos valores citados em https://docs.python.org/2/library/zlib.html#zlib.decompress
            Neste documento é mencionado um range de -15 a 47 com alguns "buracos", mas já foi visto que
            é possível a descompressão, em pelo menos um caso, com wbits=16 ou 32, que não estão no range 
            documentado, por isso são tentados todos os valores entre -15 e 47'''
            for wbits in range(47,-16,-1):
                try:
                    sd.body=zlib.decompress(sd.body, wbits)
                    break
                except:
                    continue
        scket.close()
        #job = job2.clone()
        # NOTE no more cloning, we now lock the job elsewhere to provent invalid access 
        job = job2
        job.lock()
        job.http_async_handler = _async_stage_final
        job.defer = False
        job.result = InternalResponse()
        job.result.text = sd.body
        job.result.status_code = p.get_status_code()
    
        logging.debug('status code %s', job.result.status_code)
        logging.debug('sched result %s', job.result.__class__.__name__)
        job.unlock()
        job.async.queue_job(job)


