#-*- coding: utf-8 -*-

"""
Módulo para envio de emails através do async.
"""

from dendron.async1.http_async import InternalResponse, async_http_json

def __check_structure(struct, config):
    """
    Função para validar se um dicionário contém todas as chaves requeridas
    para funcionamento.

    Caso falte alguma chave obrigatória, ela será devolvida como segundo
    valor de retorno. O primeiro será um booleano indicando se o conteúdo
    recebido é verdadeiro ou não.
    """
    if isinstance(struct, dict) and isinstance(config, dict):
        missing = [k for k in struct if k not in config]

        if len(missing) == 0:
            return True, []
        else:
            return False, missing
    else:
        return False, []



def async_email(params):
    """
    Função responsável por fazer o envio de emails utilizando o async1 interno
    para envio em segundo plano.

    :param params: Deve ser um dicionário de variáveis, contendo,
                   obrigatoriamente, as variáveis:
                   - from
                   - server
                   - port
                   - user
                   - pass
                   - subject
                   - body
                   - target

                   Pode-se enviar arquivos em anexo ao email adicionando duas
                   outras variáveis ao argument:
                   - attachment
                   - filename
    """
    mandatory = {
        'from': 'string', 'server': 'string', 'port': 'string',
        'subject': 'string', 'body': 'string', 'target': 'string',
        'user': 'string', 'pass': 'string'
    }

    missing_fields = __check_structure(mandatory, params)

    if params is None or missing_fields[0] == False:
        ret_value = InternalResponse()
        ret_value.status_code = -1
        ret_value.text = 'campos obrigatorios nao encontrados: ' + \
                ','.join(missing_fields[1])

        return ret_value

    return async_http_json('email', 'email', params)



if __name__ == "__main__":
    s = {'a': 'string', 'b': 'string'}
    c = {'c': 1, 'b': 'ola', 'a': 'teste'}
    mail_data =  {
            'teste': 1,
            'from': 'info@axoon.com.br',
            'server': 'smtp.office365.com',
            'port': '587',
            'subject': 'assunto',
            'body': 'ola?',
            'target': 'rodrigosfreitas@gmail.com',
            'user': 'info@axoon.com.br',
            'pass': '1qaz@WSX'
            }

    r = async_email(**mail_data)
    print r.status_code
    print r.text


