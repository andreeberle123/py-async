
from threading import Lock
from threading import Condition
from collections import deque

from async1 import ppool
from async1 import lasyncio

from greenlet import greenlet

class JobGreenlet(greenlet):
    def __init__(self,job):
        self._job_cb = job._job_cb
        self._job_arg = job._job_arg
        self._job = job

    def run(self):
        self._job_cb(self._job_arg)

    def get_job(self):
        return self._job

class Job(object):
    def __init__(self, async, job_callback, arg):
        self._job_cb = job_callback
        self._job_arg = arg
        self.defer = False
        self.http_async_handler = None
        self.method = None
        self.stage = 0
        self._greenlet = None
        self._mutex = Lock()

        # Don't confuse this with job_callback, callback is called when
        # job is finished.
        self.callback = None
        self.callback_args = None

        # Every job must hold the async object to access its queue everywhere.
        self.async = async


    def lock(self):
        self._mutex.acquire()

    def unlock(self):
        self._mutex.release()

    def run(self):
        self._job_cb(self._job_arg )

    def execute(self, parent_greenlet):
        """
        Executes a job. The new system now uses greenlets, which means this method
        will do one of either:
        1) in first run, it will create a greenlet, and switch to its context. The
        greenlet will then execute _job_cb() passing _job_arg as argument.
        2) in subsequent runs, it will merely switch back to the greenlet, which
        will resume from whichever it was.

        Eventual yielding will switch back to parent_greenlet

        param parent_greenlet here is the greenlet who is calling this job. It is required,
        since it is the greenlet we will defer execution to if we yield during the
        job
        """
        # NOTE careful here, we are assuming we run always on the same thread
        # (pool thread), change this if we wont have thread affinity
        self._parent_greenlet = parent_greenlet
        if (self._greenlet is None):
            # we only create a greenlet in first run. In the next runs, we simply
            # defer to it
            self._greenlet = JobGreenlet(self)
        self._greenlet.switch()

    def clone(self):
        j = Job(self.async, self._job_cb, self._job_arg)

        j.method = self.method
        j.callback = self.callback
        j.callback_args = self.callback_args
        j.stage = self.stage
        j._greenlet = self._greenlet

        return j

    def get_original_arg(self):
        return self._job_arg

    def set_entrypoint(self, callback, args):
        """
        Sets an entrypoint for this job. The next time this job runs,
        it will start at the set callback (if the callback argument isnt None).

        REMARKS

        Please notice that this mechanism belongs to the overall bot message
        handling system. This means that the job entry is actually the wrapper
        function inside the driver caller, and the wrapper is the one actually
        redirecting to the callback
        """
        self.callback = callback
        self.callback_args = args

    def job_yield(self):
        self._parent_greenlet.switch()
        


class Async(object):
    """
    The class to hold asynchronous HTTP communication between chatbots and
    servers.

    At least one instance of this class must exist inside a chatbot
    communication.
    """
    def __init__(self):
        self._list = deque()
        self._queue_mutex = Lock()
        self._queue_cond = Condition(self._queue_mutex)
        ppool.init_pool(self, 1)
        self.io = lasyncio.InternalIO()
        self.io.start()


    def queue_job(self, job):
        self._queue_cond.acquire()
        self._list.append(job)
        self._queue_cond.notify()
        self._queue_cond.release()
        return


    def pool_queue(self):
        self._queue_cond.acquire()

        while not self._list:
            self._queue_cond.wait()

        job = self._list.pop()
        self._queue_cond.release()

        return job


    def get_lio(self):
        return self.io

