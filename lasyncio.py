import select
import time
import os
import threading
import socket
import logging

from collections import deque
from threading import Lock

MODE_RECV = 1
MODE_CONNECT = 2 
MODE_CLOSE = 3 

class QueueObject:
    def __init__(self,sck,mode,callback):
        self._mode = mode
        self.sck = sck
        self._cb = callback


class InternalIO(threading.Thread):
    def __init__(self):
        super(InternalIO, self).__init__()
        self.daemon = True
        self.name = 'InternalIOThread'
        self._epoll = select.epoll()
        self._opqueue = deque()
        self._pipe_in, self._pipe_out = os.pipe()
        self._llock = Lock()

    def run(self):
        flist = dict()
        self._epoll.register(self._pipe_in,select.EPOLLIN | select.EPOLLHUP |  select.EPOLLERR | select.EPOLLOUT)        
        while 1:
            self._llock.acquire()
            if self._opqueue:
                for s in self._opqueue:
                    if s._mode == MODE_CONNECT:
                        self._epoll.register(s.sck.fileno(),select.EPOLLOUT | select.EPOLLERR |  select.EPOLLHUP)
                        flist[s.sck.fileno()] = s
                    elif s._mode == MODE_RECV:
                        self._epoll.register(s.sck.fileno(), select.EPOLLERR | select.EPOLLIN | select.EPOLLHUP)
                        flist[s.sck.fileno()] = s
                self._opqueue.clear()

            self._llock.release()
            events = self._epoll.poll(maxevents=128)
            for fileno,event in events:
                matched = False
                if event & select.EPOLLHUP or event & select.EPOLLERR:
                    # operation failed over the socket
                    s = flist[fileno]
                    errcode = s.sck.getsockopt(socket.SOL_SOCKET,socket.SO_ERROR)
                    remove = s._cb(s.sck,errcode)
                    if remove:
                        self._epoll.unregister(fileno)
                    continue
                if event & select.EPOLLOUT:
                    # socket has connected
                    logging.debug('socket connected')
                    #self._epoll.modify(fileno,select.EPOLLIN | select.EPOLLHUP |  select.EPOLLERR) 
                    s = flist[fileno]
                    remove = s._cb(s.sck,0)
                    if remove:
                        self._epoll.unregister(fileno) 
                        del flist[fileno]
                if event & select.EPOLLIN:
                    # socket is ready to be read
                    if (fileno == self._pipe_in):
                        # async wake on the thread, this means we have maintenance operation to perform,
                        # ie, adding or removing sockets from queue
                        os.read(self._pipe_in,4)
                    else:
                        try:
                            data = flist[fileno].sck.recv(8192*8) 
                        except socket.error,c:
                            flist[fileno]._errcb(flist[fileno].sck,c[0])
                            continue
                        logging.debug('read %i' % len(data))
                        if len(data) == 0:
                            #conn closed
                            del flist[fileno]
                            self._epoll.unregister(fileno)
                            s._cb(s.sck, data)
                        else:
                            s = flist[fileno]
                            s._cb(s.sck, data)

                #print("spurious wake on epoll (%d)" % event)
                #self._epoll.unregister(fileno)

    def push_socket(self,socket,callback,recv_mode,error_cb=None):
        self._llock.acquire()
        if recv_mode:
            q = QueueObject(socket,MODE_RECV,callback)
        else:
            q = QueueObject(socket,MODE_CONNECT,callback)

        if error_cb:
            q._errcb = error_cb

        self._opqueue.append(q)
        os.write(self._pipe_out,'0')
        self._llock.release()

    def close_socket(self, socket):
        self._llock.acquire()
        # FIXME: Tem algo estranho aqui... Esta variavel callback nao
        #        existe. Se cair aqui, ja era a aplicacao
        q = QueueObject(socket,MODE_CLOSE,callback)
        self._opqueue.append(q)
        os.write(self._pipe_out,'0')
        self._llock.release()


    def wake(self):
        os.write(self._pipe_out,'0')



