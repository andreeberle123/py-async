from greenlet import greenlet
import threading
import time
import logging

class PoolThread(threading.Thread):
    def __init__(self, name, async_object):
        super(PoolThread, self).__init__()
        self.name = name
        self.daemon = True
        self.async = async_object

    def g_run(self):
        while 1:
            job = self.async.pool_queue()
            job.execute(self._greenlet)

    def run(self):
        # NOTE we must start the greenlet inside the thread, since it affines
        # itself to the current thread
        self._greenlet = greenlet(self.g_run)
        self._greenlet.switch()




def init_pool(async, max_threads):
    logging.info('Initializing pool')
    threading.stack_size(1024*1024)

    for i in xrange(0, max_threads):
        t = PoolThread('ExecutorThread %d' % i, async)
        t.start()



